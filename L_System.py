import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
from matplotlib import collections  as mc

class draw_:
    def __init__(self):
        self.lines = []
        self.angle = 0
        self.x = np.array([0])
        self.y = np.array([0])
        self.direction = [1,0]
        self.queue = []#store Position and angle here

    def forward(self, length):
        new_point = np.array([self.x[-1], self.y[-1]]) + length * self.direction
        self.lines.append([(self.x[-1], self.y[-1]), (new_point[0],new_point[1])])
        self.x = np.hstack((self.x, new_point[0]))
        self.y = np.hstack((self.y, new_point[1]))

    def turn(self, angle):
        self.angle += angle
        rad = np.deg2rad(self.angle)
        self.direction = np.array([np.cos(rad), np.sin(rad)])

    def move(self, length):
        new_point = np.array([self.x[-1], self.y[-1]]) + length * self.direction
        self.x = np.hstack((self.x, new_point[0]))
        self.y = np.hstack((self.y, new_point[1]))

    def add_branch(self, none):
        self.queue.append([self.x[-1], self.y[-1], self.angle])

    def jump_branch(self, none):
        [x, y, self.angle] = self.queue.pop()
        self.x = np.hstack((self.x, x))
        self.y = np.hstack((self.y, y))

    def get_Extremes(self):
        x_min = np.min(self.x)
        x_max = np.max(self.x)
        y_min = np.min(self.y)
        y_max = np.max(self.y)
        return(x_min, x_max, y_min, y_max)

class Koch_Curve:
    def __init__(self, Iterations = 5):
        self.L_Sys = draw_()
        self.Rules = {
            "F":"F+F-F-F+F",
            "+":"+",
            "-":"-"
        }

        self.Trans = {
            "F": (self.L_Sys.forward, 1),
            "+": (self.L_Sys.turn,90),
            "-": (self.L_Sys.turn,-90)
        }
        self.Initial_Conditions = "F"
        self.Iterations = Iterations

class Dragoncurve:
    def __init__(self, Iterations = 12):
        self.L_Sys = draw_()
        self.Rules = {
            "F":"F+G",
            "G":"F-G",
            "+":"+",
            "-":"-"
        }

        self.Trans = {
            "F": (self.L_Sys.forward, 1),
            "G": (self.L_Sys.forward, 1),
            "+": (self.L_Sys.turn,90),
            "-": (self.L_Sys.turn,-90)
        }
        self.Initial_Conditions = "F"
        self.Iterations = Iterations

class C_Curve:
    def __init__(self, Iterations = 12):
        self.L_Sys = draw_()
        self.Rules = {
            "F":"+F--F+",
            "+":"+",
            "-":"-"
        }

        self.Trans = {
            "F": (self.L_Sys.forward, 1),
            "+": (self.L_Sys.turn,45),
            "-": (self.L_Sys.turn,-45)
        }
        self.Initial_Conditions = "F"
        self.Iterations = Iterations


class Sierpinski:
    def __init__(self, Iterations = 6):
        self.L_Sys = draw_()
        self.Rules = {
            "F":"F-G+F+G-F",
            "G":"GG",
            "+":"+",
            "-":"-"
        }

        self.Trans = {
            "F": (self.L_Sys.forward,1),
            "G": (self.L_Sys.forward,1),
            "+": (self.L_Sys.turn,120),
            "-": (self.L_Sys.turn,-120)
        }
        self.Initial_Conditions = "F-G-G"
        self.Iterations = Iterations

class Hilbert_Curve:
    def __init__(self, Iterations = 6):
        self.L_Sys = draw_()
        self.Rules = {
            "A":"+BF-AFA-FB+",
            "B":"-AF+BFB+FA-",
            "F":"F",
            "+":"+",
            "-":"-"
        }

        self.Trans = {
            "F": (self.L_Sys.forward,1),
            "+": (self.L_Sys.turn,90),
            "-": (self.L_Sys.turn,-90)
        }
        self.Initial_Conditions = "A"
        self.Iterations = Iterations

class Plant:
    def __init__(self, Iterations = 6):
        self.L_Sys = draw_()
        self.Rules = {
            "X":"F+[[X]-X]-F[-FX]+X",
            "F":"FF",
            "[":"[",
            "]":"]",
            "+":"+",
            "-":"-"
        }

        self.Trans = {
            "F": (self.L_Sys.forward,1),
            "[": (self.L_Sys.add_branch, None),
            "]": (self.L_Sys.jump_branch, None),
            "+": (self.L_Sys.turn,25),
            "-": (self.L_Sys.turn,-25)
        }
        self.Initial_Conditions = "X"
        self.Iterations = Iterations

class Custom:
    def __init__(self, rules: list = ["A[+B]", "[A-]B", "+[C--C]+"], turns: list = [90,-90], Initial_Condition: str = "A", Iterations: int = 8):
        self.L_Sys = draw_()
        self.rules = rules
        self.angles = turns
        self.Rules = {
            "A":self.rules[0],
            "B":self.rules[1],
            "C":self.rules[2],
            "+":"+",
            "-":"-"
        }

        self.Trans = {
            "A": (self.L_Sys.forward, 1),
            "B": (self.L_Sys.forward, 1),
            "+": (self.L_Sys.turn, self.angles[0]),
            "-": (self.L_Sys.turn, -self.angles[1])
        }
        self.Initial_Conditions = Initial_Condition
        self.Iterations = Iterations

class make_Fractals:
    def __init__(self, all = False, custom = None, Fractal_number: int = 999) -> None:
        self.Fractal_List = [Koch_Curve(), Dragoncurve(), C_Curve(), Sierpinski(), Hilbert_Curve(), Plant()]
        self.fig, self.ax1 = plt.subplots()
        self.lines = []
        self.lc = mc.LineCollection(self.lines, linewidths=1, color = 'black')
        self.ax1.add_collection(self.lc)

        if all:
            self.all_Frac()
        else:
            if Fractal_number < len(self.Fractal_List):
                self.create(self.Fractal_List[Fractal_number])
        if custom != None:
            self.custom = custom
            self.fig.subplots_adjust(top = 0.99, bottom = 0.25, left=0.07, right=0.97)
            self.alpha = Slider(ax=plt.axes([0.1, 0.07, 0.8, 0.03]), label=r"$\alpha$", valmin=-180, valmax=180, valinit=90)
            self.beta = Slider(ax=plt.axes([0.1, 0.02, 0.8, 0.03]), label=r"$\beta$", valmin=-180, valmax=180, valinit=90)
            self.Iter = Slider(ax=plt.axes([0.1, 0.12, 0.8, 0.03]), label=r"$I$", valmin=1, valmax=20, valinit=self.custom.Iterations)
            self.alpha.on_changed(self.update)
            self.beta.on_changed(self.update)
            self.Iter.on_changed(self.update)
            self.create(custom)
        plt.show()
    
    def update(self, val: float):
        self.create(Custom(turns = [self.alpha.val, self.beta.val], Iterations = int(self.Iter.val)))

    def all_Frac(self):
        for Fractal in self.Fractal_List:
            self.create(Fractal)

    def iterate(self, start, Fractal):
        out = ''
        for char in start:
            if char in Fractal.Rules:
                string_ = Fractal.Rules[char]
                out += string_
        return out

    def read_draw(self, string_, Fractal):
        for char in string_:
            if char in Fractal.Trans:
                func, val = Fractal.Trans[char]
                func(val)

    def create(self, Fractal):
        s = Fractal.Initial_Conditions
        for i in range(Fractal.Iterations):
            s = self.iterate(s, Fractal)
        self.read_draw(s, Fractal)
        self.draw(Fractal)
        return s

    def draw(self, Fractal):
        x_min, x_max, y_min, y_max = Fractal.L_Sys.get_Extremes()
        #self.ax1.clear()
        self.ax1.set_xlim([x_min-1, x_max+1])
        self.ax1.set_ylim([y_min-1, y_max+1])
        #self.ax1.plot(Fractal.L_Sys.x, Fractal.L_Sys.y)
        self.lines = Fractal.L_Sys.lines
        #print(self.lines)
        self.lc.set_segments(self.lines)
        self.fig.canvas.draw()
        #print(self.lc.get_segments())

def out_key(string_to_out):
    import pyautogui
    import time
    Trans = {
            "F": 'w',
            "G": 'w',
            "+": 'left',
            "-": 'right'
        }
    for char in string_to_out:
        out = Trans[char]
        sleeptime = 1.75
        if out == 'w':
            sleeptime = 0.4
        pyautogui.keyDown(out)
        time.sleep(sleeptime)
        pyautogui.keyUp(out)
    print("done")

if __name__ == '__main__':
    #make_all = make_Fractals(all=True)
    make = make_Fractals(Fractal_number=5)

    Custom_Frac = Custom(turns = [90, 90], Iterations = 12)
    make = make_Fractals(custom = Custom_Frac)
